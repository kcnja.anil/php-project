function showCustomer(str) {
    var xhttp;
    if (str == "") {
        document.getElementById("subs").innerHTML = "";
        return;
    }
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var studentArray = JSON.parse(this.responseText);
            document.getElementById("std").value = studentArray['name'];

            document.getElementById("dep").value = studentArray['department'];
            
            let subject = studentArray['subjects'];

            let varString = "";

            for (let i = 0; i < subject.length; i++) {
                varString += "<option>" + subject[i] + "</option>";
            }
            document.getElementById("subject").innerHTML = varString;
            console.log(subject);

        }
    };
    xhttp.open("GET", "check.php?q=" + str, true);
    xhttp.send();
}

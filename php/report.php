<?php
session_start();
require_once "../php/db.php";
require_once "../php/header.php";
echo "<title>Report</title>";
?>

<h2>Student Reports</h2>
<form method="post"  class="full-form report-form" id="report-form">
    <input type="radio" value="EC" id="EC" name="department">
    <label for="EC">EC</label>

    <input type="radio" value="CS" id="CS" name="department">
    <label for="CS">CS</label>

    <input type="radio" value="IT" id="IT" name="department">
    <label for="IT">IT</label><br>
    </form>
    <button name="get-department" form="report-form">GET REPORT</button>

    <?php
        if(isset($_POST['get-department'])){
            if(isset($_POST['department'])) {
                $GLOBALS['department']=$_POST['department'];
                $departmentQuery = "SELECT dep_id FROM tbl_Department WHERE dep_name = '".$GLOBALS['department']."';";
                $departmentData = pg_query($dbConn,$departmentQuery);
                $departmentId = pg_fetch_result($departmentData,0,'dep_id');
            }
        }
    ?>
    <table>
    <tr>
    <th>Department</th>
    <th colspan="8"><?php echo $GLOBALS['department'] ?></th>
    </tr>
    <tr>
    <th>#</th>
    <th>Student ID</th>
    <th>Subject 1</th>
    <th>Subject 2</th>
    <th>Subject 3</th>
    <th>Total Marks</th>
    <th>Average Marks</th>
    <th>Grade</th>
    <th>Rank</th>
    </tr>
    <?php 

        $reportQuery = "select std_id,        
		max(CASE WHEN rn = 1 THEN sub_name END) sub_01,
        max(CASE WHEN rn = 1 THEN sub_mark END) mark_01,
        max(CASE WHEN rn = 2 THEN sub_name END) sub_02, 
        max(CASE WHEN rn = 2 THEN sub_mark END) mark_02,
        max(CASE WHEN rn = 3 THEN sub_name END) sub_03,
        max(CASE WHEN rn = 3 THEN sub_mark END) mark_03,
		round(avg(sub_mark),2) as average,sum(sub_mark) as total,
		case when avg(sub_marK)>69 then 'A' WHEN avg(sub_mark)>59 then 'B' WHEN avg(sub_mark)>49 then 'C' ELSE 'D' END
		AS GRADE,RANK () OVER ( 
		ORDER BY sum(sub_mark) DESC
	) RANK 
		FROM (
    select *,Row_number() over(partition by std_id ) rn
    from tbl_marks 
)tbl_report where std_id in(select std_id from tbl_student where dep_id = {$departmentId}) group by std_id order by sum(sub_mark) desc;";

        $reportData = pg_query($dbConn,$reportQuery);
        $row = pg_num_rows($reportData);
        $reportDetails = pg_fetch_all($reportData,PGSQL_ASSOC);
        $i=0;
        $x=0;
        while($i<$row){
            echo "<tr><td>".++$x."</td>";
            echo "<td>".$reportDetails[$i]['std_id']."</td>";
            echo "<td>".$reportDetails[$i]['mark_01']."</td>";
            echo "<td>".$reportDetails[$i]['mark_02']."</td>";
            echo "<td>".$reportDetails[$i]['mark_03']."</td>";
            echo "<td>".$reportDetails[$i]['total']."</td>";
            echo "<td>".$reportDetails[$i]['average']."</td>";
            echo "<td>".$reportDetails[$i]['grade']."</td>";
            echo "<td>".$reportDetails[$i]['rank']."</td></tr>";
            $i++;
        }

        echo "<br>Admin is: ".$_SESSION['username']; 
    ?>
    </table>
    <style>
    
    #report-link{
        display: none;
    }
        </style>
    <?php

require_once "../php/footer.php";
    ?>


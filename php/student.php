<?php
session_start();
require_once "../php/db.php";
require_once "../php/header.php";
echo "<title>Student</title>";
?>

<h2>Enter Student Details</h2>
    <form method="post" id="student-dep" class="full-form student-form">
    <button name="create">CREATE</button><button name="update-delete-view">MORE OPTIONS</button>
    <div class="department">
        <label for="EC">EC</label>
        <input type="radio" id="EC" name="department" value='EC'>
        <label for="CS">CS</label>
        <input type="radio" id="CS" name="department" value='CS'>
        <label for="IT">IT</label>
        <input type="radio" id="IT" name="department" value='IT'>
    </div>
    
    <br>

        <label for="stdname">Student Name: </label>
        <input type="text" id="std-name" placeholder="YOUR NAME" name="stdname"><br>
        <label for="stdjoin">Date of joining: </label>
        <input type="date" id="join-date"  name="stdjoin"><br>
        

    
    <?php

            if(isset($_POST['create'])){
              echo '</form><button name="ok" form="student-dep">SAVE</button>';
            }
            if(isset($_POST['update-delete-view'])){
                echo '<label for="slno">Student Serial Number: </label><input type="int" id="slno" placeholder="Serial Number" name="slno"></form><br><div class="buttons"><button class="line" name="update" form="student-dep">UPDATE</button><button class="line"  name="delete"form="student-dep">DELETE</button><button class="line"  name="view" form="student-dep">VIEW</button></div>';
            }
              if(isset($_POST['ok'])){
                if(isset($_POST['stdname'])&& isset($_POST['stdjoin'])&&isset($_POST['department'])){

                    $department = $_POST['department'];
                    $depQuery = "SELECT dep_id FROM tbl_Department where dep_name = '$department';"; 

                    $depData = pg_query($dbConn,$depQuery);
                    $depId = pg_fetch_result($depData,0,'dep_id'); // fetch the dep_id for the department
                    $stdJoin = $_POST['stdjoin'];
                    $date = date("y",strtotime($stdJoin));
                    $newId = $date.$department;
                    $stdName=$_POST['stdname'];
                    $createQuery = "INSERT INTO tbl_Student VALUES('".$newId."'||trim(to_char(nextval('student_id_".$department."'),'000')),'".$stdName."',$depId,'$department','".$stdJoin."','".$_SESSION['username']."',current_date,nextval('sl_nos'));";
                    $createData = pg_query($dbConn,$createQuery);

              }
                    
                }
            

                if(isset($_POST['update'])){



                    if(isset($_POST['stdname'])&& isset($_POST['stdjoin'])&&isset($_POST['department'])&&isset($_POST['slno'])){
                        $department = $_POST['department'];
                        $depQuery = "SELECT dep_id FROM tbl_Department where dep_name = '$department';";
                        $depData = pg_query($dbConn,$depQuery); 
                        $depId = pg_fetch_result($depData,0,'dep_id'); // fetch the dep_id for the department
                        $stdName=$_POST['stdname'];
                        $stdJoin=$_POST['stdjoin'];
                        $stdId = $_POST['stdid'];
                        $date = date("y",strtotime($stdJoin));
                        $newId = $date.$department;
                        $slNo = $_POST['slno'];
                        $createQuery = "UPDATE tbl_Student SET std_id ='".$newId."'||trim(to_char(nextval('student_id_".$department."'),'000')), std_name = '".$stdName."',dep_id = $depId,dep_name = '$department',date_join = '".$stdJoin."',created_by = '".$_SESSION['username']."',created_on = current_date WHERE sl_no = '$slNo';";
                        $createData = pg_query($dbConn,$createQuery);
                    }
                }
                if(isset($_POST['delete'])){

                    if(isset($_POST['slno'])){
                        $slNo = $_POST['slno'];
                        $createQuery = "DELETE FROM tbl_Student WHERE sl_no = '$slNo';";
                        $createData = pg_query($dbConn,$createQuery);

    

                    }
                }
                if(isset($_POST['view'])){
                    if(isset($_POST['slno'])){
                        $slNo = $_POST['slno'];
                        $createQuery = "SELECT * FROM tbl_Student WHERE sl_no = '$slNo';";
                        $createData = pg_query($dbConn,$createQuery);
                        
                        $studentView = pg_fetch_array($createData,0,PGSQL_ASSOC);
                        echo "<div class='view-std'><b>STUDENT DETAILS:</b><br><b>Student ID: </b>".$studentView['std_id']."<br><b>Student Name: </b>".$studentView['std_name']."<br><b>Department: </b>".$studentView['dep_name']."<br><b>Date of Joining: </b>".$studentView['date_join']."</div>";
                        
                    }
                } 
            
            
        

        echo "</form><br>Admin is: ".$_SESSION['username'];

require_once "../php/footer.php";
    ?>
        <style>
    
   
    #student-link {
        display: none;
    }
        </style>

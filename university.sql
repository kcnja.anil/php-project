PGDMP     &                    y         
   university #   12.6 (Ubuntu 12.6-0ubuntu0.20.04.1) #   12.6 (Ubuntu 12.6-0ubuntu0.20.04.1) #    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16406 
   university    DATABASE     p   CREATE DATABASE university WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_IN' LC_CTYPE = 'en_IN';
    DROP DATABASE university;
                postgres    false                        3079    16407    pgcrypto 	   EXTENSION     <   CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;
    DROP EXTENSION pgcrypto;
                   false            �           0    0    EXTENSION pgcrypto    COMMENT     <   COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';
                        false    2            �            1259    16708    sl_nos    SEQUENCE     o   CREATE SEQUENCE public.sl_nos
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
    DROP SEQUENCE public.sl_nos;
       public          postgres    false            �            1259    16706    student_id_cs    SEQUENCE     v   CREATE SEQUENCE public.student_id_cs
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.student_id_cs;
       public          postgres    false            �            1259    16704    student_id_ec    SEQUENCE     v   CREATE SEQUENCE public.student_id_ec
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.student_id_ec;
       public          postgres    false            �            1259    16597    student_id_it    SEQUENCE     v   CREATE SEQUENCE public.student_id_it
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.student_id_it;
       public          postgres    false            �            1259    16481    tbl_department    TABLE     q   CREATE TABLE public.tbl_department (
    dep_id integer NOT NULL,
    dep_name character varying(10) NOT NULL
);
 "   DROP TABLE public.tbl_department;
       public         heap    postgres    false            �            1259    16571 	   tbl_marks    TABLE     (  CREATE TABLE public.tbl_marks (
    std_id character varying(20) NOT NULL,
    std_name character varying(50) NOT NULL,
    sub_id integer NOT NULL,
    sub_name character varying(50) NOT NULL,
    sub_mark integer DEFAULT 0,
    entered_by character varying(20),
    entered_on date NOT NULL
);
    DROP TABLE public.tbl_marks;
       public         heap    postgres    false            �            1259    16516    tbl_student    TABLE     L  CREATE TABLE public.tbl_student (
    std_id character varying(20) NOT NULL,
    std_name character varying(50) NOT NULL,
    dep_id integer NOT NULL,
    dep_name character varying(10) NOT NULL,
    date_join date NOT NULL,
    created_by character varying(20) NOT NULL,
    created_on date NOT NULL,
    sl_no integer NOT NULL
);
    DROP TABLE public.tbl_student;
       public         heap    postgres    false            �            1259    16486    tbl_subject    TABLE     �   CREATE TABLE public.tbl_subject (
    sub_id integer NOT NULL,
    sub_name character varying(50) NOT NULL,
    dep_id integer NOT NULL
);
    DROP TABLE public.tbl_subject;
       public         heap    postgres    false            �            1259    16444    tbl_user    TABLE     �   CREATE TABLE public.tbl_user (
    u_id integer NOT NULL,
    username character varying(20) NOT NULL,
    password character varying(20) NOT NULL
);
    DROP TABLE public.tbl_user;
       public         heap    postgres    false            �          0    16481    tbl_department 
   TABLE DATA           :   COPY public.tbl_department (dep_id, dep_name) FROM stdin;
    public          postgres    false    204   �'       �          0    16571 	   tbl_marks 
   TABLE DATA           i   COPY public.tbl_marks (std_id, std_name, sub_id, sub_name, sub_mark, entered_by, entered_on) FROM stdin;
    public          postgres    false    207   �'       �          0    16516    tbl_student 
   TABLE DATA           s   COPY public.tbl_student (std_id, std_name, dep_id, dep_name, date_join, created_by, created_on, sl_no) FROM stdin;
    public          postgres    false    206   �(       �          0    16486    tbl_subject 
   TABLE DATA           ?   COPY public.tbl_subject (sub_id, sub_name, dep_id) FROM stdin;
    public          postgres    false    205   N)       �          0    16444    tbl_user 
   TABLE DATA           <   COPY public.tbl_user (u_id, username, password) FROM stdin;
    public          postgres    false    203   *       �           0    0    sl_nos    SEQUENCE SET     4   SELECT pg_catalog.setval('public.sl_nos', 7, true);
          public          postgres    false    211            �           0    0    student_id_cs    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.student_id_cs', 2, true);
          public          postgres    false    210            �           0    0    student_id_ec    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.student_id_ec', 5, true);
          public          postgres    false    209            �           0    0    student_id_it    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.student_id_it', 1, true);
          public          postgres    false    208            O           2606    16485 "   tbl_department tbl_department_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.tbl_department
    ADD CONSTRAINT tbl_department_pkey PRIMARY KEY (dep_id);
 L   ALTER TABLE ONLY public.tbl_department DROP CONSTRAINT tbl_department_pkey;
       public            postgres    false    204            W           2606    16588 %   tbl_marks tbl_marks_std_id_sub_id_key 
   CONSTRAINT     j   ALTER TABLE ONLY public.tbl_marks
    ADD CONSTRAINT tbl_marks_std_id_sub_id_key UNIQUE (std_id, sub_id);
 O   ALTER TABLE ONLY public.tbl_marks DROP CONSTRAINT tbl_marks_std_id_sub_id_key;
       public            postgres    false    207    207            S           2606    16520    tbl_student tbl_student_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.tbl_student
    ADD CONSTRAINT tbl_student_pkey PRIMARY KEY (std_id);
 F   ALTER TABLE ONLY public.tbl_student DROP CONSTRAINT tbl_student_pkey;
       public            postgres    false    206            U           2606    16698 !   tbl_student tbl_student_sl_no_key 
   CONSTRAINT     ]   ALTER TABLE ONLY public.tbl_student
    ADD CONSTRAINT tbl_student_sl_no_key UNIQUE (sl_no);
 K   ALTER TABLE ONLY public.tbl_student DROP CONSTRAINT tbl_student_sl_no_key;
       public            postgres    false    206            Q           2606    16490    tbl_subject tbl_subject_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.tbl_subject
    ADD CONSTRAINT tbl_subject_pkey PRIMARY KEY (sub_id);
 F   ALTER TABLE ONLY public.tbl_subject DROP CONSTRAINT tbl_subject_pkey;
       public            postgres    false    205            K           2606    16465    tbl_user tbl_user_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.tbl_user
    ADD CONSTRAINT tbl_user_pkey PRIMARY KEY (u_id);
 @   ALTER TABLE ONLY public.tbl_user DROP CONSTRAINT tbl_user_pkey;
       public            postgres    false    203            M           2606    16448    tbl_user tbl_user_u_id_key 
   CONSTRAINT     U   ALTER TABLE ONLY public.tbl_user
    ADD CONSTRAINT tbl_user_u_id_key UNIQUE (u_id);
 D   ALTER TABLE ONLY public.tbl_user DROP CONSTRAINT tbl_user_u_id_key;
       public            postgres    false    203            Z           2606    16577    tbl_marks tbl_marks_std_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.tbl_marks
    ADD CONSTRAINT tbl_marks_std_id_fkey FOREIGN KEY (std_id) REFERENCES public.tbl_student(std_id);
 I   ALTER TABLE ONLY public.tbl_marks DROP CONSTRAINT tbl_marks_std_id_fkey;
       public          postgres    false    207    206    2899            [           2606    16582    tbl_marks tbl_marks_sub_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.tbl_marks
    ADD CONSTRAINT tbl_marks_sub_id_fkey FOREIGN KEY (sub_id) REFERENCES public.tbl_subject(sub_id);
 I   ALTER TABLE ONLY public.tbl_marks DROP CONSTRAINT tbl_marks_sub_id_fkey;
       public          postgres    false    205    207    2897            Y           2606    16521 #   tbl_student tbl_student_dep_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.tbl_student
    ADD CONSTRAINT tbl_student_dep_id_fkey FOREIGN KEY (dep_id) REFERENCES public.tbl_department(dep_id);
 M   ALTER TABLE ONLY public.tbl_student DROP CONSTRAINT tbl_student_dep_id_fkey;
       public          postgres    false    204    2895    206            X           2606    16491 #   tbl_subject tbl_subject_dep_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.tbl_subject
    ADD CONSTRAINT tbl_subject_dep_id_fkey FOREIGN KEY (dep_id) REFERENCES public.tbl_department(dep_id);
 M   ALTER TABLE ONLY public.tbl_subject DROP CONSTRAINT tbl_subject_dep_id_fkey;
       public          postgres    false    2895    204    205            �      x�3�tu�2�t�2������� #�3      �   �   x����j1F�w�"/`I2�:K�#h��"t�&3��@���i��{�.�t~v�pr_�*RjX#��T��KƋE8�r�I.<Ixo���j"�D�2�V;����BΧ
�?-���#����]l�U�1X�ZsJ�`ć�B����_�l:J�F�h���(=�)&/P���W�-���Ұb:&�=�ݑG���ޭ����x��	��؎snR�%�����C�e?ٷ�%      �   �   x���1�0�����.iu*���.
�S���,mǻ��x­U�q�����%\)���t���k0��a��^��pcx���_go�|���40�[7���X7{)|�|�O~��	�����7�?%~՞ҥ�aK���XG!      �   �   x�M�K�0���S�	�'0��	&n�T:�&eJ�h���~_�GY�P��X���9t6��P��а�L���Ex�`�O8ZN�D��$9	����wr���)S?�����b��Vd��iv��0��^�����;����D��8;y42e<=��>���!�C��.�n��zEY      �   d   x�3�N,J����M4�v2�r��2�2��MLM��*r�(O�p5��t�2�*-��p���/�)����+�2��J-*���OK�	�H�01J/����� ���     